import pytest
from selenium.webdriver import Firefox
from selenium.webdriver import ActionChains
from selenium.webdriver import Remote as WebDriver
from selenium.webdriver.common.by import By


@pytest.fixture
def driver():
    driver = Firefox()
    yield driver
    driver.close()


def test_paint_1a(driver):
    driver.get("https://jspaint.app")

    canvas = driver.find_element(By.CLASS_NAME, "main-canvas")
    actions = ActionChains(driver)
    actions.click_and_hold(canvas)
    actions.move_by_offset(100, 0)
    actions.move_by_offset(0, 100)
    actions.move_by_offset(-100, 0)
    actions.move_by_offset(0, -100)
    actions.release()
    actions.perform()


def test_paint_1b(driver: WebDriver):
    driver.get("https://jspaint.app")

    canvas = driver.find_element(By.CLASS_NAME, "main-canvas")
    # Similar to the previous test, but with a single chain
    ActionChains(driver) \
        .click_and_hold(canvas) \
        .move_by_offset(100, 0) \
        .pause(1) \
        .move_by_offset(0, 100) \
        .pause(1) \
        .move_by_offset(-100, 0) \
        .pause(1) \
        .move_by_offset(0, -100) \
        .release() \
        .perform()
