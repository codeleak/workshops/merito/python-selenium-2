from time import sleep

import pytest
from selenium.webdriver import Firefox
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

import config


@pytest.fixture
def driver():
    driver = Firefox()
    yield driver
    driver.quit()


def test_global_shortcut(driver):
    driver.get(config.WEB_SAMPLES_BASE_URL + "/alerts")

    action = ActionChains(driver)
    action.key_down(Keys.CONTROL)
    action.key_down(Keys.SHIFT)
    action.send_keys('a')
    action.key_up(Keys.SHIFT)
    action.key_up(Keys.CONTROL)
    action.perform()

    sleep(1)


def test_copy_paste(driver):
    driver.get(config.WEB_SAMPLES_BASE_URL + "/alerts")

    action = ActionChains(driver)
    action.key_down(Keys.CONTROL)
    action.send_keys('c')
    action.key_up(Keys.CONTROL)
    action.perform()

    sleep(1)

    action = ActionChains(driver)
    action.key_down(Keys.CONTROL)
    action.send_keys('v')
    action.key_up(Keys.CONTROL)
    action.perform()

    sleep(1)
