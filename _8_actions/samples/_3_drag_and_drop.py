import pytest
from selenium.webdriver import Firefox
from selenium.webdriver import ActionChains
from selenium.webdriver import Remote as WebDriver
from selenium.webdriver.common.by import By


@pytest.fixture
def driver():
    driver = Firefox()
    yield driver
    driver.close()


def test_drag_and_drop(driver: WebDriver):
    driver.get('https://jqueryui.com/draggable/')
    driver.switch_to.frame(0)

    draggable = driver.find_element(By.ID, "draggable")
    ActionChains(driver) \
        .click_and_hold(draggable) \
        .pause(2) \
        .move_by_offset(100, 100) \
        .pause(2) \
        .move_by_offset(100, 100) \
        .pause(2) \
        .move_by_offset(100, -100) \
        .pause(2) \
        .move_by_offset(100, -100) \
        .release() \
        .perform()
