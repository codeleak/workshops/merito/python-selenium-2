# TODO Import required modules

import pytest


@pytest.fixture
def driver():
    # TODO Create a WebDriver instance
    # TODO Navigate to the /inputs page
    yield  # TODO: Return the WebDriver object to the test functions
    # TODO Close the WebDriver instance


# TODO Provide the WebDriver (driver) object to the test function
def test_copy_and_paste_between_input_and_text_area():
    # TODO: Write a test to copy text entered into input and paste it into the text area
    # Steps:
    # - Find the input element and click on it to focus
    # - Use ActionChains to send text to the input element
    # - Use ActionChains to select the text and copy it to the clipboard (CTRL+A, CTRL+C or CMD+A, CMD+C)
    # - Find the text area element and click on it to focus
    # - Use ActionChains to paste the text from the clipboard into the text area (CTRL+V or CMD+V)
    # - Assert that the text in the text area is the same as the text entered into the input element
    # Hints:
    # - Use the Keys class to send keyboard shortcuts
    # - Use element.get_property("value") to retrieve the value of the text area element

    pytest.fail("Test not implemented!")  # Remove this line after implementing the test.
