import os

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
SCREENSHOTS_DIR = PROJECT_ROOT + "/screenshots"
TMP_DIR = PROJECT_ROOT + "/tmp"
WEB_SAMPLES_BASE_URL = "https://qalabs.gitlab.io/web-samples/snippets/#"
