import pytest
from selenium.webdriver import Chrome
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


@pytest.fixture
def firefox():
    driver = Firefox()
    driver.get(config.WEB_SAMPLES_BASE_URL + "/dates")
    yield driver
    driver.close()


@pytest.fixture
def chrome():
    driver = Chrome()
    driver.get(config.WEB_SAMPLES_BASE_URL + "/dates")
    yield driver
    driver.close()


def test_dates_firefox(firefox):
    driver = firefox

    date_input = driver.find_element(By.CSS_SELECTOR, "[data-test-id='date-input']")
    date_input.send_keys("2024-01-01")

    time_input = driver.find_element(By.CSS_SELECTOR, "[data-test-id='time-input']")
    time_input.send_keys("12:00")

    submit = driver.find_element(By.CSS_SELECTOR, "button[type='submit']")
    submit.click()

    date_value = driver.find_element(By.CSS_SELECTOR, "[data-test-id='date-value']")
    time_value = driver.find_element(By.CSS_SELECTOR, "[data-test-id='time-value']")

    assert "2024-01-01" == date_value.text
    assert "12:00" == time_value.text


def test_dates_chrome(chrome):
    driver = chrome

    date_input = driver.find_element(By.CSS_SELECTOR, "[data-test-id='date-input']")
    # Date input format is different in Chrome
    date_input.send_keys("01/01/2024")

    time_input = driver.find_element(By.CSS_SELECTOR, "[data-test-id='time-input']")
    time_input.send_keys("12:00")

    submit = driver.find_element(By.CSS_SELECTOR, "button[type='submit']")
    submit.click()

    date_value = driver.find_element(By.CSS_SELECTOR, "[data-test-id='date-value']")
    time_value = driver.find_element(By.CSS_SELECTOR, "[data-test-id='time-value']")

    assert "2024-01-01" == date_value.text
    assert "12:00" == time_value.text
