from time import sleep

import pytest
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


@pytest.fixture
def driver():
    driver = Firefox()
    driver.get(config.WEB_SAMPLES_BASE_URL + "/inputs")
    # Set window size to simulate a need to scroll into view
    driver.set_window_size(900, 400)
    yield driver
    driver.close()


def test_reset_button_outside_view(driver):
    # Find the reset button
    reset_button = driver.find_element(By.CSS_SELECTOR, "button[type='reset']")
    # Scroll to the delete button to avoid ElementNotInteractableException
    # Note: There is other solution with ActionChains, but it may not work in some cases
    driver.execute_script("return arguments[0].scrollIntoView(true)", reset_button)
    # Wait for the animation to finish
    sleep(1)
    # Click the delete button
    reset_button.click()
    # Just to see the result
    sleep(1)
