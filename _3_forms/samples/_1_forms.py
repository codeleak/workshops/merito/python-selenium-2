from time import sleep

import pytest
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


@pytest.fixture
def driver():
    driver = Firefox()
    yield driver
    driver.close()


def test_inputs(driver):
    driver.get(config.WEB_SAMPLES_BASE_URL + "/inputs")

    text_input = driver.find_element(By.CSS_SELECTOR, "input[name='textInput']")
    text_input.clear()
    text_input.send_keys("Hello World!")

    textarea_input = driver.find_element(By.CSS_SELECTOR, "textarea[name='textareaInput']")
    textarea_input.clear()
    textarea_input.send_keys("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")

    select_input = driver.find_element(By.CSS_SELECTOR, "select[name='selectInput']")
    select_input.send_keys("Option 2")

    checkbox_input = driver.find_element(By.CSS_SELECTOR, "input[name='checkboxInput']")
    checkbox_input.click()

    number_input = driver.find_element(By.CSS_SELECTOR, "input[name='numberInput']")
    number_input.clear()
    number_input.send_keys("42")

    password_input = driver.find_element(By.CSS_SELECTOR, "input[name='passwordInput']")
    password_input.clear()
    password_input.send_keys("password123")

    radio_input = driver.find_element(By.CSS_SELECTOR, "input[value='Option 2']")
    radio_input.click()

    submit_button = driver.find_element(By.CSS_SELECTOR, "button[type='submit']")
    submit_button.click()

    sleep(2)
