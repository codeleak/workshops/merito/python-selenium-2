CSS Selectors
=============

## How to verify selectors?

- Open https://qalabs.gitlab.io/vuejs-contacts-demo and login
- Open `Chrome` or `Firefox` `DevTools`
- Execute selectors using `$$` function in `JavaScript` console: `$$("")`

### Learn more about `JavaScript` console in `DevTools`

- Chrome - https://developer.chrome.com/docs/devtools/console
- Firefox: https://developer.mozilla.org/en-US/docs/Tools/Web_Console

## Useful selectors

- `table` - match all `table` elements
- `table, tr, td` - match all `table`, `tr`, and `td`
- `.v-btn` - match all elements with class `.v-btn`
- `label+input` - match all `input`s next to the `label`
- `.v-btn.v-btn--disabled.v-btn--flat` - match all elements with specified classes
- `*.v-btn:not(.v-btn--disabled)` - match all elements with class `v-btn` but not with `v-btn--disabled`
- `#contacts-list` - match element with `contacts-list` id
- `#contacts-list table` - match all `table` elements within contacts list
- `#contacts-list table > tbody > tr` - match all `tr` 
- `button[type]` - match all `button` elements with `type` attribute
- `button[type][disabled]` - match all `button` elements with `type` and `disabled` attributes
- `button[type]:not([disabled])` - match all `button` elements with `type` and `disabled` attributes
- `button[type=button]` - match all `button` elements with `type` attribute equal to `button`
- `button[type*=tt]` - match all `button` elements with `type` attribute containing text `tt`
- `div:empty` - match all empty divs
- `a:visited` - match all visited links
- `input[type=checkbox]:checked` - match all checked checkboxes
- `tbody tr:first-child` - match first `tr` within `tbody`
- `tbody tr:last-child` - match last `tr` within `tbody`
- `tbody tr:nth-child(1)` - match first `tr` within contacts list
- `tbody tr td:nth-of-type(3)` - match all third `td` within `tr`

## Exercises

- Match search input
- Match `input` next to the label with `for=search-filter`
- Match `Download` and `Delete` buttons
- Match all `button` elements with `disabled=disabled` and `class=v-btn`
- Match all names from the contact list table. Skip header in the table
- Match `Previous page` and `Next page` buttons

Bonus:

- Match `New Contact` button and click it programmatically
- Match `Download` button and remove `v-btn--disabled` class programmatically
- Match search input and type in some text programmatically
