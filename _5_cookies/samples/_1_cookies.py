import pytest
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


# One browser for all tests in this file
@pytest.fixture(scope="module")
def driver():
    driver = Firefox()
    yield driver
    driver.close()


def test_add_cookies(driver):
    driver.get(config.WEB_SAMPLES_BASE_URL + "/cookies")

    driver.add_cookie({"name": "cookie1", "value": "value1"})
    driver.add_cookie({"name": "cookie2", "value": "value2"})

    driver.find_element(By.CSS_SELECTOR, "button.btn-primary").click()

    cookies = driver.find_elements(By.CSS_SELECTOR, "div[data-test-id='cookie']")

    assert 2 == len(cookies)
    assert "cookie1" in cookies[0].text
    assert "cookie2" in cookies[1].text


# This test will fail because the cookies are persisted between tests (scope="module" in the fixture)
# To fix this, we should clear cookies before each test or use a different scope for the fixture
def test_expect_no_cookies(driver):
    driver.get(config.WEB_SAMPLES_BASE_URL + "/cookies")

    driver.find_element(By.CSS_SELECTOR, "button.btn-primary").click()

    cookies = driver.find_elements(By.CSS_SELECTOR, "div[data-test-id='cookie']")

    assert 0 == len(cookies)
