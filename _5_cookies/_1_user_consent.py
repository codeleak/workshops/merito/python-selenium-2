# TODO Import required modules

import pytest


@pytest.fixture
def driver():
    # TODO Create a WebDriver instance
    # TODO Navigate to the /consent page
    yield  # TODO: Return the WebDriver object to the test functions
    # TODO Close the WebDriver instance


# TODO Provide the WebDriver (driver) object to the test function
def test_accept_cookies():
    # TODO: Write a test to check if clicking 'Accept' sets the correct cookie.
    # Hints:
    # - Find the 'Accept' button using WebDriver methods and click on it.
    # - Retrieve userConsent cookie (driver.get_cookie()) and assert that the expected cookie is set.

    pytest.fail("Test not implemented!")  # Remove this line after implementing the test.


# TODO Provide the WebDriver (driver) object to the test function
def test_reject_cookies():
    # TODO: Write a test to ensure that clicking 'Reject' does not set the cookie.
    # Hints:
    # - Find the 'Reject' button using WebDriver methods and click on it.
    # - Retrieve userConsent cookie (driver.get_cookie()) and assert that the expected cookie is not set.

    pytest.fail("Test not implemented!")  # Remove this line after implementing the test.
