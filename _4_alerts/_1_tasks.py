# TODO Import required modules

import pytest


@pytest.fixture
def driver():
    # TODO Create a WebDriver instance
    # TODO Navigate to the /tasks page
    # TODO Create 3 or more tasks for the tests as part of the fixture (use for loop)
    yield  # TODO: Return the WebDriver object to the test functions
    # TODO Close the WebDriver instance


# TODO Provide the WebDriver (driver) object to the test function
def test_deletes_all_tasks_only_after_confirmation():
    # TODO Implement the test.
    # Hints:
    # - Find the delete all button and click it
    # - Switch to the alert and accept it
    # - Verify that all tasks are deleted by checking "Completed" and "To Complete" badges

    pytest.fail("Not implemented! Remove this line.")


# TODO Provide the WebDriver (driver) object to the test function
def test_deletes_completed_tasks_only_after_confirmation():
    # TODO Implement the test.
    # Hints:
    # - Mark one task as completed: find all tasks and click on the second one (selector: "[data-test-id^='todo-'] div")
    # - Verify that the completed count is 1
    # - Find the delete completed button and click it
    # - Switch to the alert and accept it
    # - Verify that completed tasks are deleted by checking "Completed" and "To Complete" badges

    pytest.fail("Not implemented! Remove this line.")
