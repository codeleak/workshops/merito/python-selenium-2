import pytest
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


@pytest.fixture
def driver():
    driver = Firefox()
    driver.get(config.WEB_SAMPLES_BASE_URL + "/alerts")
    yield driver
    driver.close()


def test_confirm_accept(driver):
    driver.find_element(By.CSS_SELECTOR, "button.btn-secondary").click()

    alert = driver.switch_to.alert

    assert "Are you sure you want to proceed?" == alert.text

    alert.accept()

    assert "You clicked OK!" == driver.find_element(By.CSS_SELECTOR, ".alert-info.show").text


def test_confirm_rejects(driver):
    driver.find_element(By.CSS_SELECTOR, "button.btn-secondary").click()

    alert = driver.switch_to.alert

    assert "Are you sure you want to proceed?" == alert.text

    alert.dismiss()

    assert "You clicked Cancel!" == driver.find_element(By.CSS_SELECTOR, ".alert-info.show").text
