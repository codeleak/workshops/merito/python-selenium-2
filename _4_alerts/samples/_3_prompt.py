import pytest
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


@pytest.fixture
def driver():
    driver = Firefox()
    driver.get(config.WEB_SAMPLES_BASE_URL + "/alerts")
    yield driver
    driver.close()


def test_prompt_accept(driver):
    driver.find_element(By.CSS_SELECTOR, "button.btn-danger").click()

    alert = driver.switch_to.alert

    assert "What's on your mind?" == alert.text

    alert.send_keys("Hello, World!")
    alert.accept()

    assert "You entered: Hello, World!" == driver.find_element(By.CSS_SELECTOR, ".alert-info.show").text

