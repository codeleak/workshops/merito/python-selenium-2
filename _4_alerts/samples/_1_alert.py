import pytest
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


@pytest.fixture
def driver():
    driver = Firefox()
    yield driver
    driver.close()


def test_alert(driver):
    driver.get(config.WEB_SAMPLES_BASE_URL + "/alerts")

    driver.find_element(By.CSS_SELECTOR, "button.btn-primary").click()

    alert = driver.switch_to.alert

    assert "This is an alert!" == alert.text

    alert.accept()

    assert "You dismissed the alert!" == driver.find_element(By.CSS_SELECTOR, ".alert-info.show").text
