Test Automation with Python, Pytest and Selenium
----

> The project uses samples from https://gitlab.com/qalabs/web-samples/snippets that are deployed here: https://qalabs.gitlab.io/web-samples/snippets/

<!-- TOC -->
  * [Test Automation with Python, Pytest and Selenium](#test-automation-with-python-pytest-and-selenium)
  * [Prerequisites](#prerequisites)
  * [Setup development environment](#setup-development-environment)
  * [Import project to `PyCharm`](#import-project-to-pycharm)
  * [Troubleshooting](#troubleshooting)
    * [`PyCharm` not discovering `venv`](#pycharm-not-discovering-venv)
    * [Remove run configurations](#remove-run-configurations)
* [Upgrade project dependencies](#upgrade-project-dependencies)
  * [Python](#python)
  * [Upgrade pip and setuptools](#upgrade-pip-and-setuptools)
  * [Upgrade dependencies](#upgrade-dependencies)
<!-- TOC -->

## Prerequisites

- Git, Terminal (e.g. [cmder](https://blog.qalabs.pl/narzedzia/git-cmder/)). Recommended: cmder full on Windows
- Python 3.10+
- [PyCharm](https://blog.qalabs.pl/narzedzia/python-pycharm).
- Up-to-date `Chrome` and `Firefox` browsers

> Note: update the browsers before running tests in this project.

## Setup development environment

- Open terminal and clone the repository:

```
git clone https://gitlab.com/codeleak/workshops/merito/python-selenium-2.git
```

- Navigate to project directory:

```
cd python-selenium-2
```

- Create and activate virtual environment by running:

```
python -m venv venv
```

- Activate virtual environment by running:

_On Windows:_

```
venv\Scripts\activate.bat
```

_On macOS:_

```
source venv/bin/activate
```

- Install project dependencies:

```
pip install -r requirements.txt
```

- Verify project setup by running:

```
python -m pytest _0_env/env_test.py
```

Expected results:
- `Firefox` and `Chrome` tests executed successfully (you should actually see the browsers opening and closing)
- Screenshots created in `screenshots` directory
- In the terminal you should see that 2 out of 2 tests passed

```
❯ python -m pytest _0_env/env_test.py

===================== test session starts =====================
platform darwin -- Python 3.12.1, pytest-8.0.2, pluggy-1.4.0
collected 2 items                                                                                                                                                                                                                                                 

_0_env/env_test.py ..                                                                                                                                                                                                                                       [100%]

===================== 2 passed in 5.15s =====================
```

## Import project to `PyCharm`

- Import the project to `PyCharm` by opening the project directory.

> More details: https://blog.qalabs.pl/narzedzia/python-pycharm and https://www.jetbrains.com/help/pycharm/importing-project-from-existing-source-code.html

- Make sure `pytest` is selected as default test runner before running any test.

> Open `Settings > Python Integrated Tools` and Select `pytest` in `Testing` section:

- In the project explorer navigate to `_0_env`
- Right click on `env_test.py` and select `Run pytest in env_test.py`
- Expected results: all tests passed.

## Troubleshooting

### `PyCharm` not discovering `venv`

The virtual environment previously setup should be automatically detected by `PyCharm`. If for some reason, it was not, you must configure the project SDK manually. Follow the official tutorial on configuring the virtual environment:

https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html

One other issue might be that you have used spaces in the path of your project. Make sure you don't use spaces in the project path.

### Remove run configurations

If you happen to run the test file without `pytest` set as default test runner, you may experience issues when re-running the test. You may need to clean up previously created run configurations.

- Open `Run > Edit Configurations...`
- Remove existing run configurations
- Apply changes

# Upgrade project dependencies

> Note: This is for the project maintainer only.

## Python

- Project tested with Python 3.10 and 3.11. To upgrade and test with newer Python version with `asdf`, run:

```
asdf python install <VERSION>
asdf global python <VERSION>
python -V
```

> Note: After a macOS upgrade, you may need to reinstall Xcode Command Line Tools from the command line:

```
xcode-select --install
```

- Recreate virtual environment and re-configure PyCharm

```
rm -rf venv
python -m venv venv
source venv/bin/activate
```

## Upgrade pip and setuptools

```
pip install --upgrade pip setuptools
```

## Upgrade dependencies

- Check which dependencies are outdated:

```
pip list --outdated
```

- In `requirements.txt` file, change the version of the dependency to the latest one or remove the version constraint completely. For example, change:

```
selenium==4.18.0 to selenium==4.18.1
```

- Upgrade dependencies:

```
pip install -r requirements.txt --upgrade
```
- 
- Freeze dependencies:

```
pip freeze > requirements.txt
```
