import os
from time import sleep

import pytest
from selenium.webdriver import Firefox
from selenium.webdriver import Chrome
from selenium.webdriver import FirefoxOptions
from selenium.webdriver import ChromeOptions
from selenium.webdriver.common.by import By

import config


@pytest.fixture()
def firefox():
    options = FirefoxOptions()
    # These settings are required to download files to a controlled location
    # It is recommended to use absolute paths
    options.set_preference('browser.download.folderList', 2)
    options.set_preference('browser.download.dir', os.path.abspath(config.TMP_DIR))
    driver = Firefox(options=options)
    yield driver
    driver.close()


@pytest.fixture()
def chrome():
    options = ChromeOptions()
    # The path must be absolute, otherwise Chrome won't download the file
    options.add_experimental_option("prefs", {"download.default_directory": os.path.abspath(config.TMP_DIR)})
    driver = Chrome(options=options)
    yield driver
    driver.close()


def test_download_firefox(firefox):
    driver = firefox
    driver.get(config.WEB_SAMPLES_BASE_URL + "/download")

    driver.find_element(By.CSS_SELECTOR, "a[download='sample.csv']").click()
    sleep(1)  # Explicitly wait for the file to be downloaded


def test_download_chrome(chrome):
    driver = chrome
    driver.get(config.WEB_SAMPLES_BASE_URL + "/download")

    driver.find_element(By.CSS_SELECTOR, "a[download='sample.pdf']").click()
    sleep(1)  # Explicitly wait for the file to be downloaded
