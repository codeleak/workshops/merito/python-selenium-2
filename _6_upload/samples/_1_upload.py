import pytest
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


@pytest.fixture
def driver():
    driver = Firefox()
    driver.get(config.WEB_SAMPLES_BASE_URL + "/upload")
    yield driver
    driver.close()


def test_upload_image(driver):
    # To upload a file, use the send_keys method of the file input element.
    file_input = driver.find_element(By.CSS_SELECTOR, "input[type='file']")
    # The file path must be an absolute path.
    file_path = config.PROJECT_ROOT + "/_6_upload/avatar.png"
    file_input.send_keys(file_path)
    print(f'Uploading file from: {file_path}')
