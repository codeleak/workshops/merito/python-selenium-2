import pytest
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

import config


@pytest.fixture
def driver():
    driver = Firefox()
    driver.get(config.WEB_SAMPLES_BASE_URL + "/timer")
    yield driver
    driver.close()


def test_wait_for_condition(driver):
    timer_input = driver.find_element(By.CSS_SELECTOR, "[data-test-id='timer-input']")
    timer_input.send_keys("1")

    start_button = driver.find_element(By.CSS_SELECTOR, "[data-test-id='start-button']")
    start_button.click()

    wait = WebDriverWait(driver, 2)
    wait.until(ec.text_to_be_present_in_element((By.CSS_SELECTOR, "[data-test-id='time-left']"), "Time left: 0s 0ms"))


def test_wait_for_multiple_conditions(driver):
    timer_input = driver.find_element(By.CSS_SELECTOR, "[data-test-id='timer-input']")
    timer_input.send_keys("1")

    start_button = driver.find_element(By.CSS_SELECTOR, "[data-test-id='start-button']")
    start_button.click()

    wait = WebDriverWait(driver, 2)
    wait.until(ec.all_of(
        ec.visibility_of_element_located((By.CSS_SELECTOR, "[data-test-id='alert']")),
        ec.presence_of_element_located((By.CSS_SELECTOR, "[data-test-id='alert']")),
        ec.element_to_be_clickable((By.CSS_SELECTOR, "[data-test-id='start-button']"))
    ))


def test_wait_using_lambda(driver):
    timer_input = driver.find_element(By.CSS_SELECTOR, "[data-test-id='timer-input']")
    timer_input.send_keys("1")

    start_button = driver.find_element(By.CSS_SELECTOR, "[data-test-id='start-button']")
    start_button.click()

    wait = WebDriverWait(driver, 2)
    wait.until(lambda _: driver.find_element(By.CSS_SELECTOR, "[data-test-id='alert']").text == "Time is up!")
