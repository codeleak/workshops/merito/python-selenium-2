# TODO Import required modules

import pytest


@pytest.fixture
def driver():
    # TODO Create a WebDriver instance
    # TODO Navigate to the /progress page
    yield  # TODO: Return the WebDriver object to the test functions
    # TODO Close the WebDriver instance


# TODO Provide the WebDriver (driver) object to the test function
def test_wait_for_dialog_to_be_present():
    # TODO Write a test to wait for a dialog to be present
    # Steps:
    # - Find the duration input element and set its value to 2
    # - Find the start button and click on it
    # - Create WebDriverWait instance with a timeout of 3 seconds
    # - Use the text_to_be_present_in_element expected condition to wait for the dialog message
    # - Find the dialog close button and click on it
    # - Find the progress bar and assert that its value is 100 (aria-valuenow="100")

    pytest.fail("Test not implemented!")  # Remove this line after implementing the test.


# TODO Provide the WebDriver (driver) object to the test function
def test_wait_for_progress_to_be_100():
    # TODO Write a test to wait for the progress bar to reach 100%
    # Steps:
    # - Find the duration input element and set its value to 2
    # - Find the start button and click on it
    # - Create WebDriverWait instance with a timeout of 3 seconds
    # - Use a lambda function to wait for the progress bar to reach 100%
    # - Use element.get_attribute() method to retrieve the value of the aria-valuenow attribute

    pytest.fail("Test not implemented!")  # Remove this line after implementing the test.
