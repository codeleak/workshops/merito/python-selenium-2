from selenium.webdriver import Chrome
from selenium.webdriver import ChromeOptions

import config


def main():
    # Chrome options
    options = ChromeOptions()
    options.accept_insecure_certs = True
    options.add_argument("--headless")
    options.add_argument("--disable-gpu")

    # Init Chrome
    driver = Chrome(options=options)

    # Implicit wait for element to be found
    driver.implicitly_wait(1)

    # Wait until page is loaded
    driver.get(config.WEB_SAMPLES_BASE_URL + "/welcome")

    # Print page title
    print(f'Title: {driver.title}')
    print(f'Current URL: {driver.current_url}')

    # Close the driver
    driver.close()


# Run the main function when the script is executed
if __name__ == '__main__':
    main()
