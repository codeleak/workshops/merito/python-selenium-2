from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


def main():
    driver = Firefox()
    driver.get(config.WEB_SAMPLES_BASE_URL + "/welcome")

    # Note: find_element() method returns the first element found or throws NoSuchElementException

    element = driver.find_element(By.ID, "card")
    element = driver.find_element(By.CSS_SELECTOR, "[data-test-id=tech-stack-table] > tbody > tr:nth-child(1)")
    element = driver.find_element(By.CLASS_NAME, "custom-class")
    element = driver.find_element(By.TAG_NAME, 'p')
    element = driver.find_element(By.LINK_TEXT, "Vite")
    element = driver.find_element(By.PARTIAL_LINK_TEXT, "React")
    element = driver.find_element(By.XPATH, '//*[@data-test-id="tech-stack-table"]')

    driver.close()


if __name__ == '__main__':
    main()
