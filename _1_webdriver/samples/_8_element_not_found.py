from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

import config


def main():
    driver = Firefox()
    driver.get(config.WEB_SAMPLES_BASE_URL + "/welcome")

    # No such element exception
    try:
        driver.find_element(By.ID, "non_existing_id")
    except NoSuchElementException:
        print("Exception handled: Element not found!")

    driver.close()


if __name__ == '__main__':
    main()
