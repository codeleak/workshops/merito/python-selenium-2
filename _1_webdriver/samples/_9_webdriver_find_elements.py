from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


def main():
    driver = Firefox()
    driver.get(config.WEB_SAMPLES_BASE_URL + "/welcome")

    # Find elements returns a list
    # Each object in the list is a WebElement
    web_elements = driver.find_elements(By.CSS_SELECTOR, "tbody td")

    print(f'Number of elements found: {len(web_elements)}')

    for element in web_elements:
        print(f'Element type: {element.tag_name}. Element text: {element.text}')

    # No elements found do not raise an exception. It returns an empty list.
    web_elements_empty = driver.find_elements(By.CLASS_NAME, "non_existing_class")
    print(f'No exception. Number of elements found: {len(web_elements_empty)}')

    driver.close()


if __name__ == '__main__':
    main()
