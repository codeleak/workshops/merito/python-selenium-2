from selenium.webdriver import Firefox
from selenium.webdriver import FirefoxOptions

import config


def main():
    options = FirefoxOptions()
    options.add_argument("-headless")
    options.set_preference('app.update.enabled', False)
    options.set_preference('general.warnOnAboutConfig', False)

    driver = Firefox(options=options)

    # Implicit wait for element to be found
    driver.implicitly_wait(1)

    # Wait until page is loaded
    driver.get(config.WEB_SAMPLES_BASE_URL + "/welcome")

    # Print page title
    print(f'Title: {driver.title}')
    print(f'Current URL: {driver.current_url}')

    # Close the driver
    driver.close()


if __name__ == '__main__':
    main()
