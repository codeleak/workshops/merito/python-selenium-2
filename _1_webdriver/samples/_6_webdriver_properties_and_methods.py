from selenium.webdriver import Firefox

import config


def main():
    driver = Firefox()
    driver.get(config.WEB_SAMPLES_BASE_URL + "/welcome")

    # Maximize window
    # driver.maximize_window()

    # Set window size
    driver.set_window_size(1024, 768)

    # Save screenshot
    driver.save_screenshot(f'{config.SCREENSHOTS_DIR}/screenshot.png')

    # Delete all cookies
    driver.delete_all_cookies()

    # Execute JavaScript
    driver.execute_script("alert('Hello from Selenium!');")

    # Accept alert
    alert = driver.switch_to.alert
    alert.accept()

    # Close the driver
    driver.close()


if __name__ == '__main__':
    main()
