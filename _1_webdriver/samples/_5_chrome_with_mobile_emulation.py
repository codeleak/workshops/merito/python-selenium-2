from selenium.webdriver import Chrome
from selenium.webdriver import ChromeOptions

import config


def main():
    mobile_emulation = {
        "deviceName": "iPhone X"
    }
    # Chrome options
    chrome_options = ChromeOptions()
    chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)

    # Init Chrome driver
    driver = Chrome(options=chrome_options)

    # Implicit wait for element to be found
    driver.implicitly_wait(1)

    # Wait until page is loaded
    driver.get(config.WEB_SAMPLES_BASE_URL + "/welcome")

    # Print page title
    print(f'Title: {driver.title}')
    print(f'Current URL: {driver.current_url}')

    # Close the driver
    driver.close()


# Run the main function when the script is executed
if __name__ == '__main__':
    main()
