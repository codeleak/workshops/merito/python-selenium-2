from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


def main():
    driver = Firefox()
    driver.get(config.WEB_SAMPLES_BASE_URL + "/welcome")

    link = driver.find_element(By.LINK_TEXT, "React")

    # Properties
    print("Text: " + link.text)
    print("Is displayed?: " + str(link.is_displayed()))
    print("Is enabled?: " + str(link.is_enabled()))
    print("Attribute [data-test-id]: " + str(link.get_attribute("data-test-id")))

    # Finding within the web element
    ul = driver.find_element(By.TAG_NAME, "ul")
    first_list_item = ul.find_element(By.TAG_NAME, "li")
    print("First list item: " + first_list_item.text)

    # Take screenshot of an element
    card = driver.find_element(By.CSS_SELECTOR, ".card")
    card.screenshot(f'{config.SCREENSHOTS_DIR}/card.png')

    driver.close()


if __name__ == '__main__':
    main()
