import pytest
from selenium.webdriver import Firefox

import config


@pytest.fixture
def driver():
    driver = Firefox()
    driver.get(config.WEB_SAMPLES_BASE_URL + "/welcome")
    yield driver  # Provide the fixture value
    driver.close()  # Clean up the fixture value once the test is done


# The driver fixture is used in the test functions
# The name of the fixture is used as the parameter name
def test_1(driver):
    assert "Web Samples" == driver.title


def test_2(driver):
    assert "Web Samples" == driver.title


def test_3(driver):
    assert "Web Samples" == driver.title
