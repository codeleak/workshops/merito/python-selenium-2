import pytest
from selenium.webdriver import Firefox

import config


# One browser for all tests in this file
@pytest.fixture(scope="module")
def driver_instance():
    driver = Firefox()
    yield driver  # Provide the fixture value
    driver.close()  # Clean up the fixture value once the test is done


# Fixture for each test
@pytest.fixture
def driver(driver_instance):
    driver_instance.get(config.WEB_SAMPLES_BASE_URL + "/welcome")
    return driver_instance


# The driver fixture is used in the test functions
# The name of the fixture is used as the parameter name
def test_1(driver):
    assert "Web Samples" == driver.title


def test_2(driver):
    assert "Web Samples" == driver.title


def test_3(driver):
    assert "Web Samples" == driver.title
