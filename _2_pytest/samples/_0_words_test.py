def reverse(word):
    return word[::-1]


def test_reverse():
    assert reverse("hello") == "olleh"
    assert reverse("world") == "dlrow"
    assert reverse("python") == "nohtyp"
    assert reverse("pytest") == "tsetyp"
