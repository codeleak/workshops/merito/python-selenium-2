import pytest


# Fixture (function) with yield
@pytest.fixture
def func_fix():
    print("Function fixture is created for each function in the module")
    x = "func_fix"
    yield x
    print("Function fixture - executing tear down code...")


# Fixture (module) with yield
@pytest.fixture(scope="module")
def module_fix():
    print("Module fixture is created once in the module")
    x = "module_fix"
    yield x
    print("Module fixture - executing tear down code...")


# Use fixtures in the tests by injecting functions
def test_1(func_fix, module_fix):
    pass
