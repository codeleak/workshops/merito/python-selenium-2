import pytest


def fizz_buzz(number: int) -> str:
    if number % 3 == 0 and number % 5 == 0:
        return "FizzBuzz"
    if number % 3 == 0:
        return "Fizz"
    if number % 5 == 0:
        return "Buzz"
    return str(number)


def test_returns_fizz_when_number_is_divisible_by_3():
    # TODO Call `fizz_buzz()` with any number divisible by 3
    # TODO Assert the result is equal to "Fizz"
    pytest.fail("Not implemented! Remove this line.")


def test_returns_buzz_when_number_is_divisible_by_5():
    # TODO Call `fizz_buzz()` with any number divisible by 5
    # TODO Assert the result is equal to "Buzz"
    pytest.fail("Not implemented! Remove this line.")


def test_returns_fizzbuzz_when_number_is_divisible_by_3_and_5():
    # TODO Call `fizz_buzz()` with any number divisible by 3 and 5
    # TODO Assert the result is equal to "FizzBuzz"
    pytest.fail("Not implemented! Remove this line.")


def test_returns_number_as_string_when_number_is_not_divisible_by_3_or_5():
    # TODO Call `fizz_buzz()` with any number not divisible by neither 3 nor 5
    # TODO Assert the result is equal to that number as string
    pytest.fail("Not implemented! Remove this line.")
