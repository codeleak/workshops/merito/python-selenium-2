import pytest


class List:
    def __init__(self, items=None):
        if items is None:
            items = []
        self.items = items

    def is_empty(self):
        return len(self.items) == 0

    def add(self, item):
        self.items.insert(0, item)

    def remove(self):
        return self.items.pop(0)

    def size(self):
        return len(self.items)


def test_is_empty():
    # TODO Init empty List (list = List()), run its is_empty method and assert it returns true
    pytest.fail("Not implemented! Remove this line.")


def test_add_changes_queue_size():
    # TODO Init empty List, add element, and assert queue size has changed
    pytest.fail("Not implemented! Remove this line.")


def test_remove_changes_queue_size():
    # TODO Init List with some elements, remove element, and assert queue size has changed
    pytest.fail("Not implemented! Remove this line.")
