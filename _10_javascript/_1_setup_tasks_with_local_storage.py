# TODO Import required modules

import pytest


@pytest.fixture
def driver():
    # TODO Create a WebDriver instance
    # TODO Navigate to the /tasks page
    # TODO Add tasks to the local storage
    # Hints:
    # - Use browser to copy the JSON string from the local storage
    # - Use the execute_script method to set the local storage
    # - Use the following JavaScript function: window.localStorage.setItem('key', 'value')
    # TODO Refresh the page before yielding the WebDriver instance
    yield  # TODO: Return the WebDriver object to the test functions
    # TODO Close the WebDriver instance


# TODO Provide the WebDriver (driver) object to the test function
def test_tasks_are_read_from_local_storage_on_page_load():
    # TODO Verify that the tasks are read from the local storage on page load
    pytest.fail("Test not implemented!")  # Remove this line after implementing the test.
