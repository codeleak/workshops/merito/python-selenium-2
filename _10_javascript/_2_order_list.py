# TODO Import required modules

import pytest


@pytest.fixture
def driver():
    # TODO Create a WebDriver instance
    # TODO Navigate to the /order-list page
    yield  # TODO: Return the WebDriver object to the test functions
    # TODO Close the WebDriver instance


# TODO Provide the WebDriver (driver) object to the test function
def test_order_list_asc():
    # TODO: Write a test that orders the list in ascending order.
    # Hints:
    # - Use example of drag and drop with JavaScript from samples (samples/_3_drag_and_drop_with_javascript.py)
    # - Assert order is as expected before the changes are made
    # - Sort the list in ascending order by calling the JavaScript function
    # - Assert order is as expected after the changes are made

    pytest.fail("Test not implemented!")  # Remove this line after implementing the test.


# TODO Provide the WebDriver (driver) object to the test function
def test_order_list_desc():
    # TODO: Write a test that orders the list in descending order.
    # Hints:
    # - Use example of drag and drop with JavaScript from samples
    # - Assert order is as expected before the changes are made
    # - Assert order is as expected after the changes are made

    pytest.fail("Test not implemented!")  # Remove this line after implementing the test.
