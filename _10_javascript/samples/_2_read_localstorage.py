import json

import pytest
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


@pytest.fixture()
def driver():
    driver = Firefox()
    yield driver
    driver.close()


def test_read_from_local_storage(driver):
    driver.get(config.WEB_SAMPLES_BASE_URL + "/tasks")

    todo_input = driver.find_element(By.CSS_SELECTOR, "input[data-test-id='new-todo-input']")
    todo_input.send_keys("Remember the Milk")

    add_button = driver.find_element(By.CSS_SELECTOR, "span[data-test-id='add-todo-button']")
    add_button.click()

    value = driver.execute_script("return window.localStorage.getItem('todos');")
    todos = json.loads(value)

    print(f'Todos as JSON: \n{json.dumps(todos, indent=2)}')

    assert len(todos) == 1
    assert todos[0]["text"] == "Remember the Milk"
    assert todos[0]["completed"] is False
