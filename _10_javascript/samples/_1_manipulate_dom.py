from time import sleep

import pytest
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


@pytest.fixture()
def driver():
    driver = Firefox()
    yield driver
    driver.close()


def test_apply_visual_effects_on_click(driver):
    driver.get(config.WEB_SAMPLES_BASE_URL + "/timer")
    driver.find_element(By.CSS_SELECTOR, "input[data-test-id='timer-input']").send_keys("5")

    click(driver, "button[data-test-id='start-button']")
    sleep(1)
    click(driver, "button[data-test-id='pause-button']")
    sleep(1)
    click(driver, "button[data-test-id='start-button']")
    sleep(1)
    click(driver, "button[data-test-id='stop-button']")
    sleep(1)


def click(driver, selector):
    element = driver.find_element(By.CSS_SELECTOR, selector)
    for _ in range(2):
        # Add a yellow border to the element
        driver.execute_script("arguments[0].style.border='5px solid yellow'", element)
        sleep(0.1)  # Blink on for 100 ms
        # Remove the yellow border
        driver.execute_script("arguments[0].style.border=''", element)
        sleep(0.1)  # Blink off for 100 ms
    # Click the element after the blinking effect
    element.click()
