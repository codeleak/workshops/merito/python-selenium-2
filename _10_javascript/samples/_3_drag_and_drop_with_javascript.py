from time import sleep

import pytest
from selenium.webdriver import Firefox

import config


@pytest.fixture
def driver():
    driver = Firefox()
    yield driver
    driver.close()


def test_drag_and_drop_with_javascript_1(driver):
    driver.get(config.WEB_SAMPLES_BASE_URL + '/order-list')

    # JavaScript to simulate the drag-and-drop
    # This script finds the items by their data-test-id attributes, swaps them, and updates the component state.
    script = """
    function triggerDragAndDrop(selectorDrag, selectorDrop) {
        var dataTransfer = new DataTransfer();
    
        var eventDragStart = new DragEvent('dragstart', {
            bubbles: true,
            cancelable: true,
            dataTransfer: dataTransfer,
        });
        var eventDrop = new DragEvent('drop', {
            bubbles: true,
            cancelable: true,
            dataTransfer: dataTransfer,
        });
    
        var eventDragOver = new Event('dragover', {
            bubbles: true,
            cancelable: true,
        });
    
        var dragElement = document.querySelector(selectorDrag);
        var dropElement = document.querySelector(selectorDrop);
    
        dragElement.dispatchEvent(eventDragStart);
        dropElement.dispatchEvent(eventDragOver);
        dropElement.dispatchEvent(eventDrop);
    }
    
    // Adjust the selectors based on your application's specifics
    triggerDragAndDrop('[data-test-id="item-1"]', '[data-test-id="item-3"]');
    """
    sleep(2)
    driver.execute_script(script)
    sleep(2)  # Wait for the changes to take effect


def test_drag_and_drop_with_javascript_2(driver):
    driver.get(config.WEB_SAMPLES_BASE_URL + '/order-list')

    script = """
    function triggerDragAndDrop(selectorDrag, selectorDrop) {{
        var dataTransfer = new DataTransfer();
    
        var eventDragStart = new DragEvent('dragstart', {{
            bubbles: true,
            cancelable: true,
            dataTransfer: dataTransfer,
        }});
        var eventDrop = new DragEvent('drop', {{
            bubbles: true,
            cancelable: true,
            dataTransfer: dataTransfer,
        }});
    
        var eventDragOver = new Event('dragover', {{
            bubbles: true,
            cancelable: true,
        }});
    
        var dragElement = document.querySelector(selectorDrag);
        var dropElement = document.querySelector(selectorDrop);
    
        dragElement.dispatchEvent(eventDragStart);
        dropElement.dispatchEvent(eventDragOver);
        dropElement.dispatchEvent(eventDrop);
    }}
    
    triggerDragAndDrop('{selector_drag}', '{selector_drop}');
    """

    item_1 = '[data-test-id="item-1"]'
    item_2 = '[data-test-id="item-2"]'
    item_3 = '[data-test-id="item-3"]'
    item_4 = '[data-test-id="item-4"]'

    sleep(2)
    driver.execute_script(script.format(selector_drag=item_1, selector_drop=item_3))
    sleep(2)
    driver.execute_script(script.format(selector_drag=item_2, selector_drop=item_4))
    sleep(2)
