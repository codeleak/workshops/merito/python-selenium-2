import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.firefox.options import Options as FirefoxOptions
import config


@pytest.fixture
def firefox_driver():
    firefox_options = FirefoxOptions()
    firefox_options.set_preference("app.update.auto", False)
    firefox_options.set_preference("app.update.enabled", False)
    driver = webdriver.Firefox()
    yield driver
    driver.close()


@pytest.fixture
def chrome_driver():
    chrome_options = ChromeOptions()
    chrome_options.accept_insecure_certs = True
    # chrome_options.add_argument(f"--user-data-dir={config.TMP_DIR}/chrome-data-dir")

    driver = webdriver.Chrome(
        options=chrome_options,
        service=ChromeService())

    # Implicit wait for element to be found
    driver.implicitly_wait(1)
    yield driver
    driver.close()


def test_firefox(firefox_driver):
    # Wait until page is loaded
    firefox_driver.get(config.WEB_SAMPLES_BASE_URL)

    # Assert page title
    assert firefox_driver.title == "Web Samples"
    # Save screenshot
    firefox_driver.save_screenshot("../screenshots/test_firefox.png")


def test_chrome(chrome_driver):
    # Wait until page is loaded
    chrome_driver.get(config.WEB_SAMPLES_BASE_URL)

    # Assert page title
    assert chrome_driver.title == "Web Samples"

    # Save screenshot
    chrome_driver.save_screenshot("../screenshots/test_chrome.png")
