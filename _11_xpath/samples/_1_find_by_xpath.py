import pytest
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

import config


@pytest.fixture()
def driver():
    driver = Firefox()
    yield driver
    driver.close()


def test_find_by_text(driver):
    driver.get(config.WEB_SAMPLES_BASE_URL + "/bmi")

    # Find input elements by their placeholder text
    weight_input = driver.find_element(By.XPATH, "//input[@placeholder='Enter weight in kg']")
    weight_input.send_keys("70")

    height_input = driver.find_element(By.XPATH, "//input[@placeholder='Enter height in cm']")
    height_input.send_keys("170")

    # Find the button by its text
    calculate_button = driver.find_element(By.XPATH, "//button[text()='Calculate BMI']")
    calculate_button.click()

    # Find the result by its text
    result = driver.find_elements(By.XPATH, "//div[contains(text(), 'Your BMI is')]")
    assert len(result) == 1
