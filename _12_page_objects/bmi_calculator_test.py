import pytest

from _12_page_objects.bmi_calculator_page import BmiCalculatorPage


@pytest.fixture
def driver():
    # TODO Create a WebDriver instance
    yield  # TODO: Return the WebDriver object to the test functions
    # TODO Close the WebDriver instance


@pytest.fixture
def bmi_page(driver):
    # TODO Navigate to the /bmi page
    return BmiCalculatorPage(driver)


# TODO Provide the BmiCalculatorPage (bmi_page) object to the test function
def test_bmi_calculation_normal_weight():
    # TODO Implement the test
    # Hints:
    # - Interact only with the provided page object

    pytest.fail("Not implemented! Remove this line.")


# TODO Provide the BmiCalculatorPage (bmi_page) object to the test function
def test_bmi_calculation_underweight():
    # TODO Implement the test
    # Hints:
    # - Interact only with the provided page object

    pytest.fail("Not implemented! Remove this line.")


# TODO Provide the BmiCalculatorPage (bmi_page) object to the test function
def test_bmi_calculation_overweight():
    # TODO Implement the test
    # Hints:
    # - Interact only with the provided page object

    pytest.fail("Not implemented! Remove this line.")


def test_bmi_calculation_obesity(bmi_page):
    # TODO Implement the test
    # Hints:
    # - Interact only with the provided page object

    pytest.fail("Not implemented! Remove this line.")

# TODO Optionally: implement more scenarios
