import pytest
from selenium.webdriver import Firefox

import config
from _12_page_objects.samples.tasks_page import TasksPage


@pytest.fixture
def driver():
    driver = Firefox()
    yield driver
    driver.close()


@pytest.fixture
def tasks_page(driver):
    driver.get(config.WEB_SAMPLES_BASE_URL + "/tasks")
    return TasksPage(driver)


def test_task_create_and_complete(tasks_page):
    tasks_page.create("Task 1")
    tasks_page.create("Task 2")
    tasks_page.create("Task 3")

    tasks = tasks_page.get_tasks()
    assert tasks == ["Task 1", "Task 2", "Task 3"]

    tasks_page.complete("Task 2")
    tasks_page.toggle_completed()

    tasks = tasks_page.get_tasks()
    assert tasks == ["Task 1", "Task 3"]
