from selenium.webdriver.common.by import By


class TasksPage:
    def __init__(self, driver):
        self.driver = driver

    def create(self, task_name):
        input_field = self.driver.find_element(By.CSS_SELECTOR, "input[data-test-id='new-todo-input']")
        input_field.send_keys(task_name)
        add_button = self.driver.find_element(By.CSS_SELECTOR, "[data-test-id='add-todo-button']")
        add_button.click()

    def complete(self, task_name):
        task = self._get_task(task_name)
        if task is None:
            return
        task.find_element(By.TAG_NAME, "div").click()

    def get_tasks(self):
        tasks = self.driver.find_elements(By.CSS_SELECTOR, "[data-test-id^='todo-'] > div")
        return [task.text for task in tasks]

    def toggle_completed(self):
        button = self.driver.find_element(By.CSS_SELECTOR, "a[data-test-id='toggle-completed']")
        button.click()

    def delete_completed(self):
        delete_button = self.driver.find_element(By.CSS_SELECTOR, "a[data-test-id='delete-completed']")
        delete_button.click()

    def delete_all(self):
        delete_all_button = self.driver.find_element(By.CSS_SELECTOR, "a[data-test-id='delete-all']")
        delete_all_button.click()

    def export_as_csv(self):
        export_button = self.driver.find_element(By.CSS_SELECTOR, "a[data-test-id='export-csv']")
        export_button.click()

    def get_task_count(self):
        completed = self.driver.find_element(By.CSS_SELECTOR, "span[data-test-id='completed-count']").text
        to_complete = self.driver.find_element_by_css_selector("span[data-test-id='to-complete-count']").text
        return {'completed': int(completed), 'to_complete': int(to_complete)}

    def _get_task(self, task_name):
        xpath = f"//div[starts-with(@data-test-id, 'todo-') and starts-with(., '{task_name}')]"
        task = self.driver.find_elements(By.XPATH, xpath)
        if len(task) == 0:
            return None
        return task[0]
